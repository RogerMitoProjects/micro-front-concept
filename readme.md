# Micro Front-end: A conceptual idea

This project consists of a minimalist idea of applying the concept of Micro front-end architectural pattern. It's solely made with Vue JS 2.6 without any additional framework like [Single SPA](https://single-spa.js.org/) or [Qiankun](https://github.com/umijs/qiankun) for instance - you can check out some other appealing frameworks in this article: [11 Micro Frontends Frameworks You Should Know](https://itnext.io/11-micro-frontends-frameworks-you-should-know-b66913b9cd20).
By giving up on frameworks intended for building a micro front-end driven architecture, some limitations popped up such as sending props to the child apps that I couldn't implement in this example. Anyhow, it's just a conceptual idea, we shouldn't worry about that here 😀.

The application as a whole is composed of one parent app (wrapper app) and one child app (micro front). The parent app is responsible for fetching its children (in this case, it has one child) and sending all necessary data to them.

One crucial thing about Micro Front-end architecture is how communication is established among all the apps. In our context it's done by custom events. However, other ways are also possible, such as props, local storage or some state management library like Vuex.

**Note: Before you dive inside all that, it's desirable to have a piece of good knowledge about the micro front-end concept.**

## How does it work?

In a nutshell, the parent app (_macropapp_) gets its child (table app) by downloading it from an AWS S3 bucket through a script tag inserted inside the document body. This approach is known as Runtime integration (client-side composition), one of the most simple available out there.

Once fetched and rendered, the parent app emits an event containing `authenticationCode` data, afterwards the child in its turn has a listener for this event where the incoming data is received and assigned to the table.

> It's important to define inside the parent the containers where the children will be rendered in. Here, this is made by defining a div element that matches with the target micro front (id).

<img src="client-side-composition.png" alt="Client side composition approach" width="400">

**_A graphical overview of client side composition concept_**

> Of course, this is a simplistic approach, the most recommended would be receiving the children's bundle via a CloudFront distribution instead of getting it directly from an S3 bucket.

There are other interesting strategies to integrate the micro apps all of them with their pros and cons like dependency injection, iframe integration, server-side composition, and the list goes on.

The following picture shows up this process:

<img src="execution-flowchart.png" alt="App's execution flowchart" width="600">

**_Execution flowchart_**

## Improvements

Despite it being an initial idea, even so, I'd like to enumerate some improvements that could become it even better.

- Send initial data to the child app via props instead of events;
- Get javascript child bundle via CloudFront insted of S3, what provide us benefits such as caching, custom domaing name, encryption, edge lambdas, etc;
- Define distinct rules for children apps about its execution environment - local and production;
- Write some tests, especially the integrated ones.

## Project setup

To execute each project separately, go to the respective base folder and run the essential commands described on `Dependencies setup` and `Compiles and hot-reloads for development` sections.

To put the whole project in practice, you need to build the child app (table app) storing its bundle in some public place like an AWS S3.
You can use other alternatives to serve the generated bundle, like a local webpack server as well.

If everything goes well, you'll see the following screen when accessing localhost:8080:

<img src="app.png" alt="App appearance">

**_App appearance_**

### Dependencies setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## License

Copyright © 2021 Rogério de Oliveira.
This project is available under MIT License.

Powered by [Vue Js](https://vuejs.org/).
