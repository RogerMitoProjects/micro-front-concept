import Vue from 'vue';
import App from './App.vue';
import {
  MdApp,
  MdDrawer,
  MdToolbar,
  MdButton,
  MdIcon,
  MdList,
  MdContent,
  MdDivider,
} from 'vue-material/dist/components';

import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.config.productionTip = false;

// Vue.use(MdAppDrawer);
Vue.use(MdToolbar);
Vue.use(MdApp);
Vue.use(MdDrawer);
Vue.use(MdButton);
Vue.use(MdIcon);
Vue.use(MdList);
Vue.use(MdContent);
Vue.use(MdDivider);

Vue.config.productionTip = false;

// when the script tag is loaded, it's the last script
// on the page, so grab it and pull data off of it.
const scriptTags = document.querySelectorAll('script');
const parent = scriptTags[scriptTags.length - 1];
// let { brand, apiUrl } = parent.dataset;
console.log(parent.dataset);
new Vue({
  render: (h) => h(App),
}).$mount('#app');
