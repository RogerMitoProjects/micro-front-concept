const webpack = require('webpack');

module.exports = {
  filenameHashing: false,
  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1,
      }),
    ],
    output: {
      filename: 'table-app.js',
    },
  },
  css: {
    extract: false,
  },
};
